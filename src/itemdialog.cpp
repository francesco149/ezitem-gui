/*
	Copyright 2013 Francesco Noferi (francesco1149@gmail.com)

    This file is part of ezitem-gui.

    ezitem-gui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ezitem-gui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ezitem-gui.  If not, see <http://www.gnu.org/licenses/>.

	boost license: http://www.boost.org/users/license.html
	wxWidgets license: http://www.wxwidgets.org/about/newlicen.htm
*/

#include "itemdialog.hpp"
#include "utils.hpp"
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include "logging.hpp"

namespace ezitemgui
{
	// {{{
	// itemdialog begin
	itemdialog::itemdialog(wxWindow *parent, const wxString &title, 
		std::vector<wxString> *propertynames, std::vector<wxString> *propertyvalues)
		: wxDialog(parent, wxID_ANY, title, wxDefaultPosition, wxSize(300, 400)),
			result(wxID_CANCEL)
	{
		wxPanel *basepanel = new wxPanel(this);
		wxBoxSizer *panelsizer = new wxBoxSizer(wxVERTICAL);

		scrollablegrid *grid = new scrollablegrid(basepanel, propertynames, propertyvalues);

		wxBoxSizer *buttonsizer = new wxBoxSizer(wxHORIZONTAL);
		{
			// no need to bind these buttons to events, wxID_OK and wxID_CANCEL
			// will behave like in a messagebox
			wxButton *buttonok = new wxButton(basepanel, wxID_OK, "OK");
			wxButton *buttoncancel = new wxButton(basepanel, wxID_CANCEL, "Cancel");

			buttonsizer->Add(buttonok, 0, wxALL | wxEXPAND, 0);
			buttonsizer->Add(buttoncancel, 0, wxALL | wxEXPAND, 0);
		}

		panelsizer->Add(grid, 1, wxALL | wxEXPAND, 10);
		panelsizer->Add(buttonsizer, 0, wxALL | wxEXPAND, 10);
		basepanel->SetSizer(panelsizer);

		// show modal dialog
		Centre();
		result = ShowModal();

		// build properties map
		typedef std::map<wxString,wxTextCtrl *> textctrlmap;
		boost_foreach (const textctrlmap::value_type &it, grid->ctlmap)
		{
			wxString value = it.second->GetValue();

			if (!value.IsEmpty())
				properties[it.first] = value;
		}

		Destroy();
	}

	int itemdialog::GetResult()
	{
		return result;
	}

	void itemdialog::GetProperties(std::map<wxString,wxString> *pproperties)
	{
		pproperties->insert(properties.begin(), properties.end());
	}
	// itemdialog end
	// }}}

	// {{{
	// scrollablegrid begin
	scrollablegrid::scrollablegrid(wxWindow *parent, std::vector<wxString> *propertynames, 
		std::vector<wxString> *propertyvalues)
		: wxScrolledWindow(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVSCROLL)
	{
		wxBoxSizer *wrapper = new wxBoxSizer(wxVERTICAL);
		wxGridSizer *basesizer = new wxGridSizer(0, 2, 5, 5);

		// manually add required id field
		wxTextCtrl *idvalue = new wxTextCtrl(this, wxID_ANY);
		wxStaticText *idproperty = new wxStaticText(this, wxID_ANY, "id");

		basesizer->SetRows(basesizer->GetRows() + 1);
		basesizer->Add(idproperty, 0, wxALL | wxEXPAND, 0);
		basesizer->Add(idvalue, 0, wxALL | wxEXPAND, 0);
		ctlmap["id"] = idvalue;

		// TODO: dynamic properties and a combo box to add them
		boost_foreach (const wxString &p, *propertynames)
		{
			if (p == "id")
				continue;

			wxTextCtrl *value = new wxTextCtrl(this, wxID_ANY);
			wxStaticText *property = new wxStaticText(this, wxID_ANY, p);

			// add property row to the grid sizer
			basesizer->SetRows(basesizer->GetRows() + 1);
			basesizer->Add(property, 0, wxALL | wxEXPAND, 0);
			basesizer->Add(value, 0, wxALL | wxEXPAND, 0);
			ctlmap[p] = value;
		}

		// set property values in combo boxes if provided
		if (propertyvalues)
		{
			std::vector<wxString>::iterator it = propertynames->begin();
			boost_foreach (const wxString &v, *propertyvalues)
			{
				ctlmap[*it]->ChangeValue(v);
				dbgcode(crossutils::logging::get()->d("ezitemgui::scrollablegrid", strfmt() << *it << "=" << v));
				it++;
			}
		}

		wxStaticText *info = new wxStaticText(this, wxID_ANY, 
			"An unique id will be automatically\ngenerated if the id field is left blank");

		wrapper->Add(info, 0, wxBOTTOM | wxEXPAND, 10);
		wrapper->Add(basesizer, 1, wxALL | wxEXPAND, 0);
		SetSizer(wrapper);
		FitInside();
		SetScrollRate(5, 5);
		SetMinSize(wxSize(-1, -1)); // this might be useless as it's probabilly the default size
	}
	// scrollablegrid end
	// }}}
}
