/*
	Copyright 2013 Francesco Noferi (francesco1149@gmail.com)

    This file is part of ezitem-gui.

    ezitem-gui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ezitem-gui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ezitem-gui.  If not, see <http://www.gnu.org/licenses/>.

	boost license: http://www.boost.org/users/license.html
	wxWidgets license: http://www.wxwidgets.org/about/newlicen.htm
*/

#pragma once

#include "logging.hpp"

#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/combobox.h>
#include <wx/statbox.h>

#include <map>
#include <vector>

namespace ezitemgui
{
	class mainform; // forward declaration

	// manages the filters in the gui by dynamically adding/removing rows with a
	// combobox for the propery name, a textctrl for the filtered value and
	// a button to remove the filter (which will also be used to identify each filter)
	class filtermanager
	{
	public:
		typedef std::vector<wxString> proplist; // property list type

		// holds all of the data related to a single filter
		class filter
		{
		public: // TODO: make the ctor protected
			filter(filtermanager *parent, wxStaticBox *box,
				wxGridSizer *grid, filtermanager::proplist *choices = NULL);
			virtual ~filter();
			void hide();
			void show();
			int id() const;
			wxComboBox *propertycombo() const;
			wxTextCtrl *valuetext() const;
			wxButton *removebutton() const;

		protected:
			static const std::string tag;

			filtermanager *parent;
			wxGridSizer *grid;
			wxComboBox *property;
			wxTextCtrl *value;
			wxButton *button;
		};

		// const iterator for the filters
		typedef std::map<int, filter *>::const_iterator const_iterator;

		// callback type for the onremoved event
		typedef void ( *onremove_callback)(wxEvtHandler *instance, int id);

		/*
		ponremovedhandler is a pointer to the event handler that will handle this callback
						  (for example, your wxFrame).

		the onremoved callback is a pointer to a function with the following signature
		-----------------------------------------------
		void myonremove(wxEvtHandler *instance, int id);
		-----------------------------------------------
		that will be called upon deletion of a filter.

		instance will contain your ponremovedhandler
		id will contain the filter being removed
		*/
		filtermanager(onremove_callback onremoved, wxEvtHandler *ponremovedhandler, 
			wxBoxSizer *bsizer, wxGridSizer *grid, wxStaticBox *box);

		virtual ~filtermanager();

		const_iterator begin(); // returns a stl iterator to the beginning of the control map
		const_iterator end(); // returns a stl iterator to the end of the control map
		size_t size(); // returns the number of filters in the filter map

		bool add(filter **dst = NULL); // dynamically adds a new filter row to the gridsizer
									   // and stores it in *dst if provided
		void removebyid(int id);
		void updatepropertylist(proplist *newlist); // updates property list

	protected:
		typedef std::map<int, filter *> filtermap;

		static const std::string tag;

		crossutils::logging *log;
		onremove_callback onremoved; // callback that will fire when a filter is deleted
		wxEvtHandler *ponremovedhandler; // the event handler that will be passed to the callback
		proplist *propertylist; // list of the possible property names
		filtermap filters; // filters, indexed by remove button id

		wxBoxSizer *bsizer; // main sizer
		wxStaticBox *box; // filters parent groupbox
		wxGridSizer *grid; // the filters sizer in mainform

		void refreshlayout();
		void OnRemoveClicked(wxCommandEvent &e);
	};
}
