/*
	Copyright 2013 Francesco Noferi (francesco1149@gmail.com)

    This file is part of ezitem-gui.

    ezitem-gui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ezitem-gui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ezitem-gui.  If not, see <http://www.gnu.org/licenses/>.

	boost license: http://www.boost.org/users/license.html
	wxWidgets license: http://www.wxwidgets.org/about/newlicen.htm
*/

#pragma once

// various utilities common to lots of files

#include <boost/foreach.hpp>

// macros
#define strfmt() std::ostringstream().flush()

#if defined(DEBUG) | defined(_DEBUG)
#ifndef dbgcode
#define dbgcode(...) do { __VA_ARGS__; } while (0)
#endif
#else
#ifndef dbgcode
#define dbgcode(x)
#endif
#endif

#define boost_foreach BOOST_FOREACH
