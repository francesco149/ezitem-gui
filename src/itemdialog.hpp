/*
	Copyright 2013 Francesco Noferi (francesco1149@gmail.com)

    This file is part of ezitem-gui.

    ezitem-gui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ezitem-gui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ezitem-gui.  If not, see <http://www.gnu.org/licenses/>.

	boost license: http://www.boost.org/users/license.html
	wxWidgets license: http://www.wxwidgets.org/about/newlicen.htm
*/

#pragma once

#include <wx/wx.h>
#include <wx/scrolwin.h>
#include <vector>
#include <map>

namespace ezitemgui
{
	// this modal dialog allows the user to edit the item properties with editing/adding an item
	// NOTE: do NOT call ShowModal or anything like that on it. Instantiating it is enough.
	class itemdialog : public wxDialog
	{
	public:
		// propertyvalues must be in the same order as propertynames
		itemdialog(wxWindow *parent, const wxString &title, std::vector<wxString> *propertynames, 
			std::vector<wxString> *propertyvalues = NULL);
		int GetResult(); // returns wxOK or wxCANCEL
		void GetProperties(std::map<wxString,wxString> *pproperties); // obtain property values mapped by property name

	private:
		int result; // wxOK or wxCANCEL
		std::map<wxString,wxString> properties; // property map built as the dialog closes
	};

	class scrollablegrid : public wxScrolledWindow
	{
	public:
		scrollablegrid(wxWindow *parent, std::vector<wxString> *propertynames, 
			std::vector<wxString> *propertyvalues = NULL);
		std::map<wxString,wxTextCtrl *> ctlmap; // text boxes mapped by property name TODO: make this private
	};
}
