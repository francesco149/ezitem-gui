/*
	Copyright 2013 Francesco Noferi (francesco1149@gmail.com)

    This file is part of ezitem-gui.

    ezitem-gui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ezitem-gui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ezitem-gui.  If not, see <http://www.gnu.org/licenses/>.

	boost license: http://www.boost.org/users/license.html
	wxWidgets license: http://www.wxwidgets.org/about/newlicen.htm
*/

#pragma once

#include "logging.hpp"
#include "filtermanager.hpp"
#include "zitem.hpp"

#include <wx/app.h>
#include <wx/frame.h>
#include <wx/listctrl.h>
#include <wx/timer.h>

#include <boost/scoped_ptr.hpp>

namespace ezitemgui
{
	// represents the application and all of its windows as a whole
	// contains global information about the application and is 
	// responsible for initializing the program on startup
	class app : public wxApp
	{
	public:
		static const wxString appname; // application name
		static const wxString appver; // application version

		virtual bool OnInit(); // called on startup

	protected:
		static const std::string tag;
		crossutils::logging *log;
		static void fatal(); // displays a fatal error message and terminates the program
	};

	// a custom virtual listview that uses column names to retrieve properties 
	// values directly from the zitem.xml file
	// this ensures excellent performance with no lag whatsoever when scrolling
	// huge amounts of data
	class itemlist : public wxListView
	{
	public:
		itemlist(wxWindow *parent);
		void setsource(ezitem::item::list *items); // set the source item list of the listview
		ezitem::item::list *getsource() const; // get the source item list of the listview

		// fires when the listview is being drawn, it's used by wxListView to obtain
		// the text for the given item and column
		wxString OnGetItemText(long item, long column) const;

	protected:
		ezitem::item::list *items; // pointer to an std::vector of zitem.xml items
	};

	class mainform : public wxFrame
	{
	public:
		mainform(const wxString& title, const wxPoint& pos, const wxSize& size);

	protected:
		// menu ids, used for event handling
		enum
		{
			// file
			wxID_FILE_OPEN = 1,
			wxID_FILE_SAVE,
			wxID_FILE_SAVEAS,
			wxID_FILE_CLOSE,
		    wxID_FILE_EXIT,

		    // help
		    wxID_HELP_ABOUT,

		    // item
		    wxID_ITEM_NEW,
		    wxID_ITEM_REMOVE,
		    wxID_ITEM_REMOVEALL,
		    wxID_ITEM_EDIT,
		    wxID_ITEM_CLONE,

			// TODO: find a less ghetto way to handle dynamically created menus
		    wxID_MENU_DYNAMIC
		};

		static const std::string tag;

		crossutils::logging *log;
		ezitem::zitem z; // zitem.xml manager
		filtermanager *filters; // manages dynamic filter controls on the GUI
		bool modified; // false if the file has just been saved or loaded
		itemlist *listview; // custom virtual listview
		boost::scoped_ptr<wxTimer> refresher; // timer that refreshes the item listview
		wxComboBox *propertycombo; // mass editing property
		wxTextCtrl *valuetext; // mass editing value
		wxMenu *itemmenu; // item menu
		wxMenu *columnmenu; // column toggle menu

		void updatetitle(); // updates the window title when the file is modified etc...
		void updateitemmenu(); // updates the item menu when it opens
		bool savezitem(const wxString &path = ""); // saves the currently open xml file
		bool promptmodified(); // displays and handles the "save file before closing" message
		void closefile(); // closes the currently open file
		void updatepropertylist(); // updates the property list in comboboxes and other ctls

		// callback that fires when a filter is deleted by the filtermanager
		static void removefilterafter(wxEvtHandler *pthis, int id);
		
		void removefilter(int id); // removes a filter from the zitem manager and from the gui
		void setfilter(int id, wxString name, wxString value); // edits a filter
		void noxml(); // displays an error when no file is open

		// creates two string arrays with property names and values from the properties
		// of an item. if the item has no properties, the default property list is saved
		// with no values
		// this is used to format property data to be used in the item edit/create dialog
		void trybuildproplist(ezitem::property *pprops,
            std::vector<wxString> *pnames, std::vector<wxString> *pvalues = NULL);

		// displays the item dialog in create/clone item mode with the given properties
		void newitem(const wxString &caption, ezitem::property props = ezitem::property::null);

		// displays the item dialog in edit item mode with the given properties
		void edititem(const wxString &caption, ezitem::property props);

		// fires when a menu is clicked
		void OnItemMenuOpened(wxMenuEvent &e);

		// file menu events
		void OnFileOpenClicked(wxCommandEvent &e);
		void OnFileSaveClicked(wxCommandEvent &e);
		void OnFileSaveAsClicked(wxCommandEvent &e);
		void OnFileCloseClicked(wxCommandEvent &e);
		void OnFileExitClicked(wxCommandEvent &e);

		// help menu events
		void OnHelpAboutClicked(wxCommandEvent &e);

		// item menu events
		void OnItemNewClicked(wxCommandEvent &e);
		void OnItemRemoveClicked(wxCommandEvent &e);
		void OnItemRemoveAllClicked(wxCommandEvent &e);
		void OnItemEditClicked(wxCommandEvent &e);
		void OnItemCloneClicked(wxCommandEvent &e);

		// column menu events
		void OnColumnToggled(wxCommandEvent &e);

		// filter groupbox events
		void OnAddFilterClicked(wxCommandEvent &e);
		void OnMassEditSetClicked(wxCommandEvent &e);

		// window events
		void OnRefreshTimer(wxTimerEvent &e);
		void OnClose(wxCloseEvent& e);
	};
}
