/*
	Copyright 2013 Francesco Noferi (francesco1149@gmail.com)

    This file is part of ezitem-gui.

    ezitem-gui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ezitem-gui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ezitem-gui.  If not, see <http://www.gnu.org/licenses/>.

	boost license: http://www.boost.org/users/license.html
	wxWidgets license: http://www.wxwidgets.org/about/newlicen.htm
*/

#pragma once

#include <string>
#include <sstream>
#include <fstream>

// use this macro to define the logfile name
#define crossutils_logging_filename(file) \
namespace crossutils \
{ \
        const char * const logging::filename = file; \
}

namespace crossutils
{
    // a complete logging class I copy-pasted from one of my other projects
    class logging
    {
    public:
        enum verbosity
        {
            assert = 0,
            error = 1,
            warn = 2,
            info = 3,
            debug = 4,
            verbose = 5
        };

        virtual ~logging();
        static logging * const get(); // returns the singleton instance
        void setverbosity(const verbosity v);
        const verbosity getverbosity() const;

        const bool wtf(const std::string tag, const std::string message) const;
        const bool e(const std::string tag, const std::string message) const;
        const bool w(const std::string tag, const std::string message) const;
        const bool i(const std::string tag, const std::string message) const;
        const bool d(const std::string tag, const std::string message) const;
        const bool v(const std::string tag, const std::string message) const;

        const bool wtf(const std::string tag, const std::basic_ostream<char> &format) const;
        const bool e(const std::string tag, const std::basic_ostream<char> &format) const;
        const bool w(const std::string tag, const std::basic_ostream<char> &format) const;
        const bool i(const std::string tag, const std::basic_ostream<char> &format) const;
        const bool d(const std::string tag, const std::basic_ostream<char> &format) const;
        const bool v(const std::string tag, const std::basic_ostream<char> &format) const;

    protected:
        static const char * const tag;
        static const char * const filename;
        verbosity verb;

        logging(); // ensures there is no way to construct more than one singleton instance
        const bool openfile(std::ofstream &f, const std::fstream::openmode mode = std::fstream::out) const;
        const bool puts(const char * const text) const;
        const bool log(const verbosity v, const std::string tag, const std::string message) const;
        const bool log(const verbosity v, const std::string tag, const std::basic_ostream<char> &format) const;
    };
}
