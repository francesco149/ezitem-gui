/*
	Copyright 2013 Francesco Noferi (francesco1149@gmail.com)

    This file is part of ezitem-gui.

    ezitem-gui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ezitem-gui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ezitem-gui.  If not, see <http://www.gnu.org/licenses/>.

	boost license: http://www.boost.org/users/license.html
	wxWidgets license: http://www.wxwidgets.org/about/newlicen.htm
*/

#include "mainform.hpp"

#include "utils.hpp"
#include "itemdialog.hpp"

#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/menu.h>
#include <wx/font.h>
#include <wx/msgdlg.h>
#include <wx/statbox.h>
#include <wx/filedlg.h>

#include <algorithm>
#include <boost/bind.hpp>

// macro that defines the logfile for the logging class
crossutils_logging_filename("lastsession.log")

// a simple macro to shorten code when binding menus
#define menu_bind(functor, id) \
	Bind(wxEVT_COMMAND_MENU_SELECTED, functor, this, id);

namespace ezitemgui
{
	// {{{
	// app begin
	const wxString app::appname = "ezitem-gui";
	const wxString app::appver = "r1";
	const std::string app::tag = "ezitemgui::app";

	bool app::OnInit()
	{
		mainform *frame;

		log = crossutils::logging::get();
		dbgcode(log->setverbosity(crossutils::logging::verbose));

		// create main frame
		log->i(tag, "OnInit: initializing top level frame");
		frame = new mainform(appname, wxDefaultPosition, wxSize(500, 426));

		if (!frame) // out of memory?
		{
			log->e(tag, "OnInit: could not create top-level frame");
			fatal();
			return false;
		}

		// display top level window
		log->i(tag, "OnInit: making top-level frame visible");
		SetTopWindow(frame); // optional (I think)
		frame->Show(); // makes the main frame visible

		return true;
	}

	void app::fatal()
	{
		static const wxString msg = "A fatal error has occurred and the application "
			"will now terminate.\nPlease check the log file for more information.";

		wxLogFatalError(msg);
	}
	// app end
	// }}}

	// {{{
	// itemlist begin
	itemlist::itemlist(wxWindow *parent)
		: wxListView(parent, wxID_ANY, wxDefaultPosition,
			wxDefaultSize, wxLC_REPORT | wxLC_VIRTUAL)
	{
		SetItemCount(0); // initialize the listview as empty
	}

	wxString itemlist::OnGetItemText(long item, long column) const
	{
		wxListItem citem; // will recieve the retrieved column information
		citem.SetMask(wxLIST_MASK_TEXT); // required to properly get column text on win

		// note to self: columns will have the same names as the properties
		GetColumn(column, citem);

		return items->at(item).get(citem.GetText());
	}

	void itemlist::setsource(ezitem::item::list *items)
	{
		this->items = items;
		SetItemCount(items->size()); // setitemcount makes the listview refresh
		Refresh(); // this forces a refresh when the item count doesn't change
	}

	ezitem::item::list *itemlist::getsource() const
	{
		return items;
	}
	// itemlist end
	// }}}

	// {{{
	// mainform begin
	const std::string mainform::tag = "ezitemgui::mainform";

	mainform::mainform(const wxString& title, const wxPoint& pos, const wxSize& size)
		: wxFrame(NULL, wxID_ANY, title, pos, size),
			log(crossutils::logging::get()), 
			filters(NULL), // filter manager instance
			modified(false), // true if the currently open file has unsaved changes
			listview(NULL), // custom listview
			propertycombo(NULL), // property combobox for the mass edit function
			valuetext(NULL) // property value for the mass edit function
	{
		wxMenu *menu; // will be reused for all menus

		// load icon (optional on windows, as it's embedded in the resources)
		wxImage::AddHandler(new wxPNGHandler); // enable PNG loader
		wxIcon ico; 
		wxBitmap bitmap("zapp.png", wxBITMAP_TYPE_PNG); // load icon as a wxBitmap
		ico.CopyFromBitmap(bitmap); // build icon from the bitmap
		SetIcon(wxIcon(ico)); // finally set the icon

		// font size
		log->i(tag, "mainform: setting font size");
		wxFont font(GetFont()); // clone current font and resize it
		font.SetPointSize(9); // set font size
		SetFont(font); // replace current font

		log->i(tag, "mainform: initializing controls");
		// base panel for the basic window background with a sizer
		// for controls and nested boxes
		wxPanel *basepanel = new wxPanel(this);
		wxBoxSizer *basesizer = new wxBoxSizer(wxVERTICAL);

		// create menu bar
		wxMenuBar *mbar = new wxMenuBar;

		// file menu
		menu = new wxMenu;
		menu->Append(wxID_FILE_OPEN, "Open");
		menu->Append(wxID_FILE_SAVE, "Save");
		menu->Append(wxID_FILE_SAVEAS, "Save as...");
		menu->Append(wxID_FILE_CLOSE, "Close");
		menu->Append(wxID_FILE_EXIT, "Exit");
		mbar->Append(menu, "File"); // add menu to the menu bar

		// bind menu events
		menu_bind(&mainform::OnFileOpenClicked, wxID_FILE_OPEN);
		menu_bind(&mainform::OnFileSaveClicked, wxID_FILE_SAVE);
		menu_bind(&mainform::OnFileSaveAsClicked, wxID_FILE_SAVEAS);
		menu_bind(&mainform::OnFileCloseClicked, wxID_FILE_CLOSE);
		menu_bind(&mainform::OnFileExitClicked, wxID_FILE_EXIT);

		// help menu
		menu = new wxMenu;
		menu->Append(wxID_HELP_ABOUT, wxString::Format("About %s", app::appname));
		mbar->Append(menu, "Help");

		// bind menu events
		menu_bind(&mainform::OnHelpAboutClicked, wxID_HELP_ABOUT);

		// item menu
		menu = new wxMenu;
		menu->Append(wxID_ITEM_NEW, "New...");
		menu->Append(wxID_ITEM_REMOVE, "Remove");
		menu->Append(wxID_ITEM_REMOVEALL, "Remove all filtered");
		menu->Append(wxID_ITEM_EDIT, "Edit");
		menu->Append(wxID_ITEM_CLONE, "Clone");
		mbar->Append(menu, "Item");
		itemmenu = menu;

		// bind menu events
		menu_bind(&mainform::OnItemNewClicked, wxID_ITEM_NEW);
		menu_bind(&mainform::OnItemRemoveClicked, wxID_ITEM_REMOVE);
		menu_bind(&mainform::OnItemRemoveAllClicked, wxID_ITEM_REMOVEALL);
		menu_bind(&mainform::OnItemEditClicked, wxID_ITEM_EDIT);
		menu_bind(&mainform::OnItemCloneClicked, wxID_ITEM_CLONE);
		Bind(wxEVT_MENU_OPEN, &mainform::OnItemMenuOpened, this); // will keep the menu updated

		// column menu
		columnmenu = new wxMenu;
		mbar->Append(columnmenu, "Columns");

		// add menu bar to frame
		SetMenuBar(mbar);

		// status bar (the thing at the bottom of the window)
		CreateStatusBar();

		// all of these controls don't need to be deallocated
		// why? because wxWidgets uses some weird, confusing custom memory management
		// so some classes are automatically deallocated by wxWidgets, which is the
		// case for top-level window frames and all of its children

		// item listview
		// NOTE: apparently it's illegal to set a staticbox as a parent
		// 		 (which is probabilly what was fucking up the alignment
		//		 on linux). A staticboxsizer must be created instead.
		wxStaticBoxSizer *listbox = new wxStaticBoxSizer(wxVERTICAL,
			basepanel, "Loaded zitem.xml");
		{
			wxStaticBox *box = listbox->GetStaticBox();

			// controls
			dbgcode(log->d(tag, "mainform: initializing item box"));
			listview = new itemlist(box);

			// add controls to sizer
			dbgcode(log->d(tag, "mainform: adding item box controls to sizer"));
			listbox->Add(listview, 1, wxALL | wxEXPAND, 10);
		}

		// add/remove/edit filters
		wxStaticBoxSizer *filterbox = new wxStaticBoxSizer(wxVERTICAL,
			basepanel, "Filters");
		{
			wxStaticBox *box = filterbox->GetStaticBox();

			// controls
			dbgcode(log->d(tag, "mainform: initializing filter box"));
			wxButton *button = new wxButton(box, wxID_ANY, "Add new");
			wxGridSizer *grid = new wxGridSizer(0, 3, 5, 5); // filter list
			filters = new filtermanager(removefilterafter, this, basesizer, grid, box); // init filter manager

			// sizer
			dbgcode(log->d(tag, "mainform: adding filter box controls to sizer"));
			filterbox->Add(button, 0, wxALL | wxEXPAND, 10);
			filterbox->Add(grid, 1, wxALL | wxEXPAND, 10);

			// controls binds
			button->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &mainform::OnAddFilterClicked, this);
		}

		// mass editing
		wxStaticBoxSizer *masseditingbox = new wxStaticBoxSizer(wxVERTICAL,
			basepanel, "Set in all filtered items");
		{
			wxStaticBox *box = masseditingbox->GetStaticBox();

			// controls
			dbgcode(log->d(tag, "mainform: initializing mass editing box"));
			wxGridSizer *grid = new wxGridSizer(1, 3, 5, 5);
			propertycombo = new wxComboBox(box, wxID_ANY, "property");
			valuetext = new wxTextCtrl(box, wxID_ANY, "value");
			wxButton *button = new wxButton(box, wxID_ANY, "Set");

			// sizer
			grid->Add(propertycombo, 0, wxALL | wxEXPAND, 0);
			grid->Add(valuetext, 0, wxALL | wxEXPAND, 0);
			grid->Add(button, 0, wxALL | wxEXPAND, 0);
			masseditingbox->Add(grid, 1, wxALL | wxEXPAND, 10);

			// controls binds
			button->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &mainform::OnMassEditSetClicked, this);
		}

		// main sizer
		dbgcode(log->d(tag, "mainform: adding main panel boxes to sizer"));
		basesizer->Add(listbox, 1, wxALL | wxEXPAND, 10); // the listview groupbox needs to resize
		basesizer->Add(filterbox, 0, wxRIGHT | wxLEFT | wxBOTTOM | wxEXPAND, 10);
		basesizer->Add(masseditingbox, 0, wxRIGHT | wxLEFT | wxBOTTOM | wxEXPAND, 10);
		basepanel->SetAutoLayout(true);
		basepanel->SetSizer(basesizer);
		basepanel->Layout(); // fixes the layout snapping into place after the first resize

		// bind close event handler
		Bind(wxEVT_CLOSE_WINDOW, &mainform::OnClose, this);

		// create filters refresh timer and bind it
		refresher.reset(new wxTimer(this, wxID_ANY));
		Bind(wxEVT_TIMER, &mainform::OnRefreshTimer, this);
		refresher->Start(1000); // 1 second

		updateitemmenu();

		wxLogStatus("Idle.");
	}

	// updates the window caption according to the currently opened file
	void mainform::updatetitle()
	{
		wxString t = wxString::Format("%s%s - %s",
			z.currentfilename(), modified ? "*" : "", app::appname);
		SetTitle(t);
	}

	// toggles item menu items on and off according to selected items in the listview
	void mainform::updateitemmenu()
	{
		if (!itemmenu->GetMenuItemCount()) // this might happen when closing the app
			return;

		if (listview->GetFirstSelected() == -1)
		{
			// no item selected, disable edit/remove clone
			itemmenu->Enable(wxID_ITEM_EDIT, false);
			itemmenu->Enable(wxID_ITEM_REMOVE, false);
			itemmenu->Enable(wxID_ITEM_CLONE, false);
		}
		else
		{
			// item selected, enable edit/remove clone
			itemmenu->Enable(wxID_ITEM_EDIT, true);
			itemmenu->Enable(wxID_ITEM_REMOVE, true);
			itemmenu->Enable(wxID_ITEM_CLONE, true);
		}
	}

	bool mainform::savezitem(const wxString &path)
	{
		// TODO: create backup file

		if (!z.save(path))
		{
			wxLogStatus("Failed to write %s", z.currentfilename());
			wxLogError("Could not save file! Try running the application as admin.");
			return false;
		}

		modified = false;
		updatetitle();
		wxLogStatus("Successfully saved to %s", z.currentfilename());

		return true;
	}

	// returns true if the file was not modified
	// or if the modified file was saved successfully
	bool mainform::promptmodified()
	{
		if (!modified)
			return true;

		// current file was modified, prompt user to save it
		int res = wxMessageBox(
			wxString::Format("Do you want to save changes to %s?", z.currentfilename()),
			app::appname, wxICON_QUESTION | wxYES_NO | wxCANCEL, this);

		if (res == wxCANCEL)
			return false;

		else if (res == wxYES)
		{
			if (!savezitem())
				return false;
		}

		return true;
	}

	void mainform::closefile()
	{
		promptmodified();

		z.close();
		listview->setsource(&z.filtered);
		updateitemmenu();
		modified = false;
	}

	// updates every controls that is supposed to display the property list
	// TODO: find a way to derive a class from wxComboBox and make the property list virtual
	void mainform::updatepropertylist()
	{
		typedef filtermanager::proplist proplist; // shortens code
		wxString oldvalue = propertycombo->GetValue();

		propertycombo->Clear(); // clear combo property list

		// clear column menu
		// TODO: save columns settings
		while (columnmenu->GetMenuItemCount())
			columnmenu->Delete(columnmenu->FindItemByPosition(0));

		listview->ClearAll(); // delete items and columns

		size_t i = 0;
		boost_foreach (const wxString &p, *z.properties())
		{
			// append new choices
			propertycombo->Append(p);

			// append column menu item
			columnmenu->AppendCheckItem(wxID_MENU_DYNAMIC + i, p);
			menu_bind(&mainform::OnColumnToggled, wxID_MENU_DYNAMIC + i);
			// TODO: find a better way to bind dynamic menus

			// check and add default columns
			if (p == "id" || p == "name" || p == "res_level")
			{
				columnmenu->Check(wxID_MENU_DYNAMIC + i, true);
				listview->AppendColumn(p);
			}

			i++;
		}

		// restore old combo value
		propertycombo->SetValue(oldvalue);
	}

	void mainform::removefilterafter(wxEvtHandler *phandler, int id)
	{
		// this triggers when a filter in the filter manager gets removed
		// I'm using CallAfter to avoid deleting the filter inside the event handler
		// for the remove button because otherwise it causes crashes and undefined
		// behaviour
		mainform *pthis = reinterpret_cast<mainform *>(phandler);
		pthis->CallAfter(&mainform::removefilter, id);
	}

	// called by filtermanager when a filter is removed
	// TODO: find a cleaner way to handle this stuff within filtermanager
	void mainform::removefilter(int id)
	{
		dbgcode(log->d(tag, strfmt() << "removefilter: removing filter " << id));
		filters->removebyid(id); // remove filter from GUI
		z.removefilter(id); // remove filter from zitem manager
		listview->setsource(&z.filtered); // update listview
	}

	// called when a filter is modified
	void mainform::setfilter(int id, wxString name, wxString value)
	{
		z.setfilter(id, name, value);
		listview->setsource(&z.filtered);
	}

	void mainform::noxml()
	{
		// TODO: implement creation of a zitem.xml from scratch
		wxMessageBox("Please open your zitem.xml first!",
			app::appname, wxICON_WARNING, this);
	}

	void mainform::OnItemMenuOpened(wxMenuEvent &e)
	{
		// update item menu before opening it
		updateitemmenu();
		e.Skip(); // not sure if this is really necessary
	}

	void mainform::OnFileOpenClicked(wxCommandEvent &e)
	{
		// prompt for unsaved changes
		if (!promptmodified())
			return;

		wxLogStatus("Selecting zitem.xml");

		// create and display open file dialog as a modal window
		wxFileDialog ofd(this, "Open zitem.xml", "", "", "XML files (*.xml)|*.xml",
			wxFD_OPEN | wxFD_FILE_MUST_EXIST);

		if (ofd.ShowModal() == wxID_CANCEL)
			return;

		// get selected file path
		wxString path = ofd.GetPath();

		if (!z.load(path))
		{
			wxLogStatus("Failed to open %s", path);
			wxLogError("Could not open file! Try running the application as admin.");
			return;
		}

		z.refreshproperties(); // refresh item property list
		filters->updatepropertylist(z.properties()); // refresh item prop list in the filter manager
		updatepropertylist(); // refresh property list in existing gui controls
		listview->setsource(&z.filtered); // refresh listview

		// set to no unsaved changes and update title
		modified = false;
		updatetitle();

		wxLogStatus("Successfully opened %s (%lu items)", z.currentfilename(), z.size());
	}

	void mainform::OnFileSaveClicked(wxCommandEvent &e)
	{
		savezitem();
	}

	void mainform::OnFileSaveAsClicked(wxCommandEvent &e)
	{
		wxLogStatus("Selecting save file location");

		// create and show save file dialog as a modal dialog
		wxFileDialog sfd(this, "Save zitem.xml", "", "", "XML files (*.xml)|*.xml",
			wxFD_SAVE | wxFD_OVERWRITE_PROMPT);

		if (sfd.ShowModal() == wxID_CANCEL)
			return;

		// get selected save path
		wxString path = sfd.GetPath();

		if (!savezitem(path))
			return;

		// set to no unsaved changes and update title
		modified = false;
		updatetitle();

		wxLogStatus("Successfully saved %s", path);
	}

	void mainform::OnFileCloseClicked(wxCommandEvent &e)
	{
		closefile();
	}

	void mainform::OnFileExitClicked(wxCommandEvent &e)
	{
		wxLogStatus("Terminating");
		Close(false);
	}

	void mainform::OnHelpAboutClicked(wxCommandEvent &e)
	{
		wxString ver = wxString::Format("%s %s", app::appname, app::appver);

		wxMessageBox(
			wxString::Format("%s\n\n"
				"coded by Francesco \"Franc[e]sco\" Noferi\n"
				"http://sabishiimedia.wordpress.com/\n"
				"francesco1149@gmail.com", ver),
			ver, wxICON_INFORMATION | wxOK, this
		);
	}

	void mainform::trybuildproplist(ezitem::property *pprops,
        std::vector<wxString> *pnames, std::vector<wxString> *pvalues)
	{
		// if properties are provided, create property value list
		ezitem::property prop = *pprops;

		// no item properties provided, get global property list 
		if (!prop.valid())
		{
			pnames->insert(pnames->begin(), z.properties()->begin(), z.properties()->end());
			return;
		}

		dbgcode(log->d(tag, "trybuildproplist: building property list from item properties"));
		// iterate through item properties
		while (prop.valid())
		{
			pnames->push_back(prop.name()); // add prop name to names list

			// add prop value to values list if provided
			if (pvalues)
			{
                pvalues->push_back(prop.value());
				dbgcode(log->v(tag, strfmt() << "trybuildproplist: " << prop.name() << 
					"=" << prop.value() << " | total values: " << pvalues->size()));
			}

			prop = prop.next();
		}

		dbgcode(log->v(tag, strfmt() << "trybuildproplist: returning with pvalues->size()=" 
			<< pvalues->size() << " and pvalues=" << pvalues));
	}

	void mainform::newitem(const wxString &caption, ezitem::property props)
	{
		// we can't create new items unless we have a zitem.xml opened
		if (!z.ready())
		{
			noxml();
			return;
		}

		int res; // result of the item dialog
		std::map<wxString, wxString> properties; // property map (for the zitem manager)
		std::vector<wxString> names; // property names (for the item dlg)
		std::vector<wxString> values; // property values (for the item dlg)

		// build names and values lists from the given item properties
		trybuildproplist(&props, &names, &values);

		// display modal item dialog
		itemdialog idlg(this, caption, &names, &values);
		res = idlg.GetResult(); // get result of the dialog (ok/cancel)

		if (res == wxID_CANCEL)
		{
			wxLogStatus("User canceled");
			return;
		}

		// get property map from the user input done in itemdialog
		idlg.GetProperties(&properties);

		// check for id uniqueness or generate one
		if (properties.find("id") != properties.end())
		{
			if (!z.isuniqueid(properties["id"]))
			{
				wxLogStatus("Operation failed");
				wxLogError("The item id must be unique!");
				return;
			}
		}
		else
			properties["id"] = z.getuniqueid();

		z.add(&properties); // add item to zitem.xml
		listview->setsource(&z.filtered); // refresh listview

		wxLogStatus(wxString::Format("Successfully added item %s with %lu properties!",
			properties["id"], properties.size()));

		// set to unsaved changes and update item menu and title
		updateitemmenu();
		modified = true;
		updatetitle();
	}

	void mainform::edititem(const wxString &caption, ezitem::property props)
	{
		// we can't edit items unless we have a zitem.xml opened
		if (!z.ready())
		{
			noxml();
			return;
		}

		int res; // result of the item dialog
		std::map<wxString,wxString> properties; // property map (for the zitem manager)
		std::vector<wxString> names; // property names (for the item dlg)
		std::vector<wxString> values; // property values (for the item dlg)
		wxString oldid; // old item id (in case the user decides to change it)

		dbgcode(log->d(tag, strfmt() << "edititem: building property list on &values=" << &values));

		// build names and values lists from the given item properties
		trybuildproplist(&props, &names, &values);

		dbgcode(log->d(tag, strfmt() << "edititem: sending " << names.size() << 
			" properties with " << values.size() << " values from " << &values << " to itemdialog"));

		// get position of the id property in the property names
		std::vector<wxString>::iterator itid =
            std::find_if(names.begin(), names.end(), boost::bind(std::equal_to<wxString>(), _1, "id"));

		// obtain old id from the property values
		if (itid != names.end())
		{
			size_t index = itid - names.begin();
			
			if (values.size() - 1 < index)
			{
				log->wtf(tag, "edititem: property names and values returned by trybuilproplist don't match");
				wxLogStatus("Operation failed");
				return;
			}

			oldid = values[index];
		}
		else
		{
			log->wtf(tag, "edititem: selected item is missing an id");
			wxLogStatus("Operation failed");
			wxLogError("The selected item does not contain an id. Your zitem.xml might be corrupted or incorrect.");
			return;
		}

		// display modal item dialog
		itemdialog idlg(this, caption, &names, &values);
		res = idlg.GetResult(); // get item dialog result (ok/cancel)

		if (res == wxID_CANCEL)
		{
			wxLogStatus("User canceled");
			return;
		}

		// get property map from the user input done in itemdialog
		idlg.GetProperties(&properties);

		// if the item id changed, check for id uniqueness or generate one
		// if the new item id is empty
		std::map<wxString, wxString>::iterator itnewid = properties.find("id"); // id property pos
		if (itnewid != properties.end())
		{
			// if the id was changed and it's not unique...
			if (itnewid->second != oldid && !z.isuniqueid(properties["id"]))
			{
				wxLogStatus("Operation failed");
				wxLogError("The item id must be unique!");
				return;
			}
		}
		else
			properties["id"] = z.getuniqueid();

		// edit item in zitem.xml
		if (!z.edit(oldid, &properties))
			wxLogError("Something went wrong! Please report this bug");

		// refresh listview
		listview->setsource(&z.filtered);

		wxLogStatus(wxString::Format("Successfully edited item %s which now has %lu properties!",
			properties["id"], properties.size()));

		// set to unsaved changes and update item menu and title
		updateitemmenu();
		modified = true;
		updatetitle();
	}

	void mainform::OnItemNewClicked(wxCommandEvent &e)
	{
		wxLogStatus("Creating new item...");
		newitem("Create new item");
	}

	void mainform::OnItemEditClicked(wxCommandEvent &e)
	{
		// get selected item and display item dialog
		long sel = listview->GetFirstSelected();
		wxLogStatus("Editing item...");
		edititem("Edit selected item", z.filtered[sel].getproperties());
	}

	void mainform::OnItemCloneClicked(wxCommandEvent &e)
	{
		// get selected item and display item dialog
		long sel = listview->GetFirstSelected();
		wxLogStatus("Cloning item...");
		newitem("Clone item", z.filtered[sel].getproperties());
	}

	void mainform::OnItemRemoveClicked(wxCommandEvent &e)
	{
		unsigned long selcount = 0; // selected item count
		long sel = listview->GetFirstSelected(); // index of the first selected item

		// get selected item count
		while (sel != -1)
		{
			selcount++;
			sel = listview->GetNextSelected(sel);
		}

		int res = wxMessageBox(
			wxString::Format("This will delete %lu selected items! Are you sure?", selcount),
			app::appname, wxICON_WARNING | wxYES_NO, this
		);

		if (res == wxNO)
			return;

		// re-get first selected item index
		sel = listview->GetFirstSelected();

		// iterate all selected indices and delete them
		// can't loop through the indices because by deleting them we're shifting
		// each item upwards, so it's always the first index which needs to be deleted
		for (unsigned long i = 0; i < selcount; i++)
			z.filtered_remove(sel);

		// refresh listview, set to unsaved changes and update item menu and title
		listview->setsource(&z.filtered);
		updateitemmenu();
		modified = true;
		updatetitle();
	}

	void mainform::OnItemRemoveAllClicked(wxCommandEvent &e)
	{
		// only possible if a zitem.xml is open
		if (!z.ready())
		{
			noxml();
			return;
		}

		int res = wxMessageBox(
			wxString::Format("This will delete all of the filtered items (%lu items)! "
				"Are you sure?", z.filtered_size()),
			app::appname, wxICON_WARNING | wxYES_NO, this
		);

		if (res == wxNO)
			return;

		if (!z.filtercount()) // no filters?
			z.clear(); // saves some iterations
		else
			z.filtered_clear();

		// refresh listview, set to unsaved changes and update item menu and title
		listview->setsource(&z.filtered);
		updateitemmenu();
		modified = true;
		updatetitle();
	}

	void mainform::OnColumnToggled(wxCommandEvent &e)
	{
		int menuitem = e.GetId(); // id of the menu item that was toggled

		dbgcode(log->d(tag, strfmt() <<
			"OnColumnToggled: handling menu id " << menuitem));

		if (e.IsChecked()) // column activated
		{
			// append toggled column to listview
			dbgcode(log->d(tag, strfmt() <<
				"OnColumnToggled: appending column " << columnmenu->GetLabel(menuitem)));
			listview->AppendColumn(columnmenu->GetLabel(menuitem));
			return;
		}

		// find and delete disabled column
		wxListItem citem;

		for (int column = 0; column < listview->GetColumnCount(); column++)
		{
			listview->GetColumn(column, citem);
			citem.SetMask(wxLIST_MASK_TEXT); // required to properly get column text on win

			if (citem.GetText() == columnmenu->GetLabel(menuitem))
			{
				listview->DeleteColumn(column);
				break;
			}
		}
	}

	void mainform::OnAddFilterClicked(wxCommandEvent &e)
	{
		filtermanager::filter *flt = NULL;

		if (filters->add(&flt)) // add filter to gui
		{
			// add filter to zitem manager
			z.addfilter(
				flt->id(),
				flt->propertycombo()->GetValue(),
				flt->valuetext()->GetValue()
			);

			// refresh listview
			listview->setsource(&z.filtered);
			wxLogStatus("Successfully added new filter.");
		}

		updateitemmenu(); // refresh item menu
	}

	void mainform::OnMassEditSetClicked(wxCommandEvent &e)
	{
		wxString property = propertycombo->GetValue(); // mass edit prop combobox selection text
		wxString value = valuetext->GetValue(); // mass edit textbox value

		int res = wxMessageBox(
			wxString::Format("This will overwrite the property '%s' in %lu items by "
				"setting it to the value '%s'. Are you sure?\nNOTE: if the property does "
				"not exist, it will be created",
				property, z.filtered_size(), value),
			app::appname, wxICON_WARNING | wxYES_NO, this
		);

		if (res == wxNO)
			return;

		z.filtered_set(property, value); // set value in all filtered items
		modified = true; // unsaved changes
		updatetitle(); // update window title
		listview->setsource(&z.filtered); // force listview refresh
	}

	// this will be called periodically to check for changes in the filters
	void mainform::OnRefreshTimer(wxTimerEvent &e)
	{
		// check the filters in the GUI for changes and refresh the actual zitem filters
		for (filtermanager::const_iterator it = filters->begin(); it != filters->end(); it++)
		{
			int id = it->first;
			wxString name = it->second->propertycombo()->GetValue();
			wxString value = it->second->valuetext()->GetValue();

			// if the filter values have changed, refresh it in the zitem manager
			if (z.getfiltername(id) != name || z.getfiltervalue(id) != value)
				setfilter(id, name, value);
		}
	}

	void mainform::OnClose(wxCloseEvent& e)
	{
		if (e.CanVeto() && !promptmodified())
		{
			e.Veto(); // cancel the close event
			return;
		}

		wxLogStatus("Performing cleanup...");

		log->i(tag, "OnClose: performing cleanup");
		// we absolutely need to delete the filtermanager early
		// this ensures that all filter controls are destroyed early, before 
		// wxWidgets attempts to delete them itself
		// TODO: see if I can leave the deallocation of the filter controls
		// to wxWidgets
		delete filters;
		filters = NULL;

		e.Skip(); // let wx handle the close event normally
	}
	// mainform end
	// }}}
}
