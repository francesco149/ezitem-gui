/*
	Copyright 2013 Francesco Noferi (francesco1149@gmail.com)

    This file is part of ezitem-gui.

    ezitem-gui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ezitem-gui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ezitem-gui.  If not, see <http://www.gnu.org/licenses/>.

	boost license: http://www.boost.org/users/license.html
	wxWidgets license: http://www.wxwidgets.org/about/newlicen.htm
*/

#pragma once

#include "logging.hpp"

#include <wx/string.h>
#include <wx/xml/xml.h>

#include <list>
#include <vector>
#include <map>

namespace ezitem
{
	// forward declarations
	class item; 
	class zitem;

	// wrapper for wxXmlAttribute
	// holds information about xml nodes' properties
	class property
	{
	public:
		static const property null;

		virtual ~property();
		bool valid();
		property next() const;
		wxString name() const;
		wxString value() const;
		void setname(const wxString &name);
		void setvalue(const wxString &value);
		bool operator==(const property& other) const;

	protected:
		friend class item;

		wxXmlAttribute *prop;
		property(wxXmlAttribute *prop);
	};

	// iterator for the item list
	class item
	{
	public:
		typedef std::vector<item> list;
		typedef list::iterator iterator;

		virtual ~item();
		wxString get(const wxString &prop) const; // obtains the value of the property
		void set(const wxString &prop, const wxString &value); // set prop (add if it doesn't exist)
		bool has(const wxString &prop) const; // check if item has a property
		property getproperties() const; // returns first property (linked list)
		bool operator==(const item& other) const;

	protected:
		friend class zitem;
		wxXmlNode *node; // pointer to the actual data
		item(wxXmlNode *node); // can only be constructed from friend classes
		wxXmlNode *data();
	};

	class zitem
	{
	public:
		// TODO: make proper accessors instead of exposing these
		item::list unfiltered; // complete items list
		item::list filtered; // filtered list, will be regenerated on filter add/edit

		zitem();
		virtual ~zitem();

		bool ready();
		bool matchfilters(const item &i); // checks wether an item matches the current filters
		bool load(const wxString &file); // initializes wxXmlDocument and the filtered/unfiltered list
		bool save(wxString filename = "");
		void close();
		size_t size(); // total item count
		size_t filtered_size(); // filtered item count
		bool isuniqueid(const wxString &id); // check whether the provided id is unique
		wxString getuniqueid(); // generate an unique id
		bool add(std::map<wxString,wxString> *properties); // add an item with the given properties
															// note: the properties must at least 
															// include an id
		bool edit(wxString id, std::map<wxString,wxString> *properties); // replace/add properties of an item
		void remove(long index); // remove item by index
		void remove(wxString id); // remove item by id
		void clear(); // remove all items
		void filtered_remove(long index); // remove filtered item by index
		void filtered_clear(); // remove all filtered items
		void set(const wxString &property, const wxString &value); // set a property in all of the items
		void filtered_set(const wxString &prop, const wxString &value); // set a prop in all filtered items
		void addfilter(int id, const wxString &prop, const wxString &value); // add a property filter
		bool setfilter(int id, const wxString &prop, const wxString &value); // edit filter by id
		wxString getfiltername(int id);
		wxString getfiltervalue(int id);
		bool removefilter(int id); // remove filter by id
		wxString currentfilename();
		std::vector<wxString> *properties();
		void refreshproperties();
		size_t filtercount();

	protected:
		// internal struct that holds data for filters
		struct filter
		{
			typedef std::list<filter> list;
			typedef list::iterator iterator;

			int id; // will be used to edit / remove filters
			wxString property;
			wxString value;
		};

		static const std::string tag;

		crossutils::logging *log;
		wxXmlDocument *doc;
		filter::list filters; // filters list
		wxString currentfile; // currently opened filename
		std::vector<wxString> proplist; // list of all items unique properties

		bool getroot(wxXmlNode **proot = NULL);
		void remove(long index, bool filtered); // remove item by index in filtered/unfiltered lists
		void setinlist(item::list &list, const wxString &property, const wxString &value);
		filter *getfilterbyid(int id);
		bool removefilterbyid(int id);
		void refreshfiltered();
	};
}