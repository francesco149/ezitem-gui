/*
	Copyright 2013 Francesco Noferi (francesco1149@gmail.com)

    This file is part of ezitem-gui.

    ezitem-gui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ezitem-gui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ezitem-gui.  If not, see <http://www.gnu.org/licenses/>.

	boost license: http://www.boost.org/users/license.html
	wxWidgets license: http://www.wxwidgets.org/about/newlicen.htm
*/

#include "filtermanager.hpp"

#include "utils.hpp"

namespace ezitemgui
{
	// {{{
	// begin filtermanager::filter
	const std::string filtermanager::filter::tag = "ezitemgui::filtermanager::filter";

	filtermanager::filter::filter(filtermanager *parent,
		wxStaticBox *box, wxGridSizer *grid, filtermanager::proplist *choices)
		: parent(parent), 
		  grid(grid)
	{
		// initialize filter row elements
		if (choices)
			property = new wxComboBox(box, wxID_ANY, "property", wxDefaultPosition,
				wxDefaultSize, choices->size(), &choices->at(0));
		else
			property = new wxComboBox(box, wxID_ANY, "property");

		value = new wxTextCtrl(box, wxID_ANY, "value");
		button = new wxButton(box, wxID_ANY, "Remove");

		// add filter row to the grid sizer
		grid->SetRows(grid->GetRows() + 1);
		grid->Add(property, 0, wxALL | wxEXPAND, 0);
		grid->Add(value, 0, wxALL | wxEXPAND, 0);
		grid->Add(button, 0, wxALL | wxEXPAND, 0);

		// bind remove button
		button->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &filtermanager::OnRemoveClicked, parent);
	}

	filtermanager::filter::~filter()
	{
		// unbind button
		button->Unbind(wxEVT_COMMAND_BUTTON_CLICKED, &filtermanager::OnRemoveClicked, parent);

		// remove the filter from the gui
		if (!grid->Detach(property))
			parent->log->e(tag, "~filter: failed to remove property from sizer");

		if (!grid->Detach(value))
			parent->log->e(tag, "~filter: failed to remove value from sizer");

		if (!grid->Detach(button))
			parent->log->e(tag, "~filter: failed to remove button from sizer");

		grid->SetRows(grid->GetRows() - 1);

		// refresh panels as usual
		parent->refreshlayout();

		// destroy controls
		// if the program happens to crash upon deleting filters
		// avoid using delete and call ->Destroy(), wxWidgets will
		// automatically delete the class instances at the best time
		delete property;
		delete value;
		delete button;
	}

	void filtermanager::filter::hide()
	{
		property->Hide();
		value->Hide();
		button->Hide();
	}

	void filtermanager::filter::show()
	{
		property->Show();
		value->Show();
		button->Show();
	}

	int filtermanager::filter::id() const
	{
		return button->GetId();
	}

	wxComboBox *filtermanager::filter::propertycombo() const
	{
		return property;
	}

	wxTextCtrl *filtermanager::filter::valuetext() const
	{
		return value;
	}

	wxButton *filtermanager::filter::removebutton() const
	{
		return button;
	}
	// end filtermanager::filter
	// }}}

	// {{{
	// begin filtermanager
	const std::string filtermanager::tag = "ezitemgui::filtermanager";

	filtermanager::filtermanager(onremove_callback onremoved, 
		wxEvtHandler *ponremovedhandler, wxBoxSizer *bsizer, 
		wxGridSizer *grid, wxStaticBox *box)
		: log(crossutils::logging::get()), 
		  onremoved(onremoved), // callback that fires when a filter is removed
		  ponremovedhandler(ponremovedhandler), // event handler passed to the onremoved callback
		  propertylist(NULL), // property list that will appear on comboboxes
		  bsizer(bsizer), // base sizer of the main window
		  box(box), // filters groupbox
		  grid(grid) // gridsizer of the filter list
	{
		dbgcode(log->d(tag, strfmt() <<
			"init: initialized filtermanager with grid=" <<
			grid << " and box=" << box));
	}

	filtermanager::~filtermanager()
	{
		dbgcode(log->d(tag, strfmt() << "~filtermanager: cleaning up filter map"));

		// free all filter instances
		// controls are unbound and deleted inside of the filter destructor
		boost_foreach (filtermap::value_type &it, filters)
		{
			dbgcode(log->d(tag, strfmt() << "~filtermanager: it={" <<
				it.first << "," << it.second << "}"));

			delete filters[it.first];
			it.second = NULL;
		}
	}

	filtermanager::const_iterator filtermanager::begin()
	{
		return filters.begin();
	}

	filtermanager::const_iterator filtermanager::end()
	{
		return filters.end();
	}

	size_t filtermanager::size()
	{
		return filters.size();
	}

	bool filtermanager::add(filter **dst)
	{
		if (!grid || !box || !bsizer)
			return false;

		dbgcode(log->d(tag, strfmt() << "add: adding a new filter row"));

		// add and index filter in the filter map and refresh layout
		filter *flt = new filter(this, box, grid, propertylist);
		filters[flt->removebutton()->GetId()] = flt;
		refreshlayout();

		// store ptr to the filter
		if (dst)
			*dst = flt;

		return true;
	}

	void filtermanager::updatepropertylist(proplist *newlist)
	{
		dbgcode(log->d(tag, strfmt() << "updatepropertylist: refreshing list"));
		propertylist = newlist;

		// refresh existing filters
		boost_foreach (filtermap::value_type &it, filters)
		{
			wxComboBox *ctl = it.second->propertycombo();
			wxString oldvalue = ctl->GetValue();

			ctl->Clear(); // clear property list

			// append new choices
			boost_foreach (const wxString &p, *propertylist)
				ctl->Append(p);

			// restore old value
			ctl->SetValue(oldvalue);
		}
	}

	void filtermanager::removebyid(int id)
	{
		// dealloc filter from the map
		delete filters[id];
		filters[id] = NULL;
		filters.erase(id);
	}

	void filtermanager::refreshlayout()
	{
		// required to refresh the sizers and panels to fit the new buttons
		bsizer->Layout();
	}

	// event handler for remove buttons
	void filtermanager::OnRemoveClicked(wxCommandEvent &e)
	{
		wxButton *b = dynamic_cast<wxButton *>(e.GetEventObject());
		filter *flt = filters[b->GetId()];

		dbgcode(log->d(tag,
			strfmt() << "OnRemoveClicked: remove button event caught" <<
			" property=" << flt->propertycombo() <<
			" value=" << flt->valuetext() <<
			" button=" << flt->removebutton())
		);

		// fire the onremoved callback
		if (onremoved)
			onremoved(ponremovedhandler, flt->id());

		dbgcode(log->d(tag, "filtermanager: this should appear before calling removefilter"));
	}
	// end filtermanager
	// }}}
}
