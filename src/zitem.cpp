/*
	Copyright 2013 Francesco Noferi (francesco1149@gmail.com)

    This file is part of ezitem-gui.

    ezitem-gui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ezitem-gui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ezitem-gui.  If not, see <http://www.gnu.org/licenses/>.

	boost license: http://www.boost.org/users/license.html
	wxWidgets license: http://www.wxwidgets.org/about/newlicen.htm
*/

#include "zitem.hpp"
#include "utils.hpp"
#include <boost/filesystem.hpp>
#include <boost/bind.hpp>

namespace ezitem
{
	// {{{
	// begin property
	const property property::null(NULL);

	property::property(wxXmlAttribute *prop)
		: prop(prop)
	{
		// empty
	}

	property::~property()
	{
		// empty
	}

	bool property::valid()
	{
		return prop != NULL;
	}

	property property::next() const
	{
		return property(prop->GetNext());
	}

	wxString property::name() const
	{
		return prop->GetName();
	}

	wxString property::value() const
	{
		return prop->GetValue();
	}

	void property::setname(const wxString &name)
	{
		prop->SetName(name);
	}

	void property::setvalue(const wxString &value)
	{
		prop->SetValue(value);
	}

	bool property::operator==(const property& other) const
	{
		return prop == other.prop;
	}
	// end property
	// }}}

	// {{{
	// begin item
	item::item(wxXmlNode *node)
		: node(node)
	{
		// empty
	}

	item::~item()
	{
		// empty
	}

	wxXmlNode *item::data()
	{
		return node;
	}

	wxString item::get(const wxString &prop) const
	{
		// returns an empty string if the prop is not set
		return node->GetAttribute(prop, "");
	}

	void item::set(const wxString &prop, const wxString &value)
	{
		wxXmlProperty *pr;

		if (!node->HasAttribute(prop))
		{
			// add property if it doesn't exist
			pr = new wxXmlAttribute(prop, value);
			// will be deallocated by wxXmlDocument
			node->AddAttribute(pr);
		}
		else
		{
			pr = node->GetAttributes();

			while (pr)
			{
				if (!pr->GetName().Cmp(prop))
					pr->SetValue(value);

				pr = pr->GetNext();
			}
		}
	}

	bool item::has(const wxString &prop) const
	{
		return node->HasAttribute(prop);
	}

	property item::getproperties() const
	{
		return property(node->GetAttributes());
	}

	bool item::operator==(const item& other) const
	{
		return node == other.node;
	}
	// end item
	// }}}

	// {{{
	// begin zitem
	const std::string zitem::tag = "ezitem::zitem";

	zitem::zitem()
		: log(crossutils::logging::get()), doc(NULL), currentfile("Untitled")
	{
		// empty
	}

	zitem::~zitem()
	{
		close();
	}

	bool zitem::ready()
	{
		return getroot();
	}

	bool zitem::matchfilters(const item &i)
	{
		bool match = false;

		if (!filters.size())
			return true;

		// check if item matches at least one filter
		boost_foreach (const filter &fi, filters)
		{
			// item matches filter
			if (i.has(fi.property) && fi.value == i.get(fi.property))
			{
				match = true;
				break;
			}
		}

		return match;
	}

	bool zitem::load(const wxString &file)
	{
		if (doc)
			close();

		// TODO: is there a better way to unload a wxXmlDocument
		// rather than reallocating the entire thing?
		doc = new wxXmlDocument;

		log->i(tag, "load: loading xml document...");
		if (!doc->Load(file))
			return false;

		wxXmlNode *root;

		if (!getroot(&root))
			return false;

		// parse zitem.xml
		log->i(tag, "load: parsing xml document...");
		wxXmlNode *child = root->GetChildren();
		currentfile = file;

		while (child)
		{
			if (!child->GetName().CmpNoCase("item"))
			{
				item i(child);

				// add to both lists
				if (matchfilters(i))
					filtered.push_back(i);

				unfiltered.push_back(i);
			}

			child = child->GetNext();
		}

		return true;
	}

	bool zitem::save(wxString filename)
	{
		if (filename.IsEmpty())
			filename = currentfile;
		else
			currentfile = filename;

		return doc->Save(filename, 4);
	}

	void zitem::close()
	{
		delete doc;
		doc = NULL;
		filtered.clear();
		unfiltered.clear();
	}

	size_t zitem::size()
	{
		return unfiltered.size();
	}

	size_t zitem::filtered_size()
	{
		return filtered.size();
	}

	bool zitem::isuniqueid(const wxString &id)
	{
		// TODO: find a more efficient way to check for uniqueness
		return
			std::find_if(unfiltered.begin(), unfiltered.end(),
				boost::bind(&item::get, _1, "id") == id)
			== unfiltered.end();
	}

	wxString zitem::getuniqueid()
	{
		// find highest id
		unsigned long max = 0;

		boost_foreach (const item &it, unfiltered)
		{
			unsigned long id;
			wxString strid = it.get("id");

			if (!strid.ToULong(&id))
			{
				log->w(tag, strfmt() <<
					"getuniqueid: found an invalid item entry with id='" << strid << "'");
				continue; // should never happen but you never know
			}

			if (id > max)
				max = id;
			else if (id == max)
			{
				log->w(tag, strfmt() <<
					"getuniqueid: found a non-unique id (" << strid << ")");
			}
		}

		// return highest id + 1
		return wxString::Format("%lu", max + 1);
	}

	bool zitem::add(std::map<wxString, wxString> *properties)
	{
		// validate properties
		if (!properties)
			return false;

		if (properties->find("id") == properties->end())
			return false;

		// validate id as an unsigned long integer
		unsigned long id;

		if (!properties->at("id").ToULong(&id))
			return false;

		wxXmlNode *root;

		if (!getroot(&root))
			return false;

		// create node and wrap it in an item object
		wxXmlNode *node = new wxXmlNode(root, wxXML_ELEMENT_NODE, "ITEM");
		item newitem(node);

		// append properties
		typedef std::map<wxString,wxString> propmap;
		boost_foreach (const propmap::value_type &it, *properties)
			newitem.set(it.first, it.second);

		// add item to xml document and to filtered and unfiltered lists
		root->AddChild(node);
		unfiltered.push_back(newitem);

		if (matchfilters(newitem))
			filtered.push_back(newitem);

		return true;
	}

	bool zitem::edit(wxString id, std::map<wxString,wxString> *properties)
	{
		log->i(tag, strfmt() << "edit: editing " << properties->size() << " properties in item " << id);

		// validate properties
		if (!properties)
		{
			log->wtf(tag, "edit: why did you pass me a null properties pointer?");
			return false;
		}

		if (properties->find("id") == properties->end())
		{
			log->e(tag, "edit: no id property provided");
			return false;
		}

		// validate id as an unsigned long integer
		unsigned long ulid;

		if (!properties->at("id").ToULong(&ulid))
		{
			log->e(tag, "edit: invalid id provided in properties");
			return false;
		}

		if (!id.ToULong(&ulid))
		{
			log->e(tag, "edit: invalid id provided");
			return false;
		}

		// find item by id
		item::list::iterator found = std::find_if(unfiltered.begin(), unfiltered.end(),
				boost::bind(&item::get, _1, "id") == id);

		if (found == unfiltered.end())
		{
			log->e(tag, "edit: the item does not exist!");
			return false;
		}

		// replace properties
		typedef std::map<wxString,wxString> propmap;
		boost_foreach (const propmap::value_type &it, *properties)
			found->set(it.first, it.second);

		return true;
	}

	void zitem::remove(long index, bool isfiltered)
	{
		wxXmlNode *root;

		if (!getroot(&root))
			return;

		item::list &a = isfiltered ? filtered : unfiltered;
		item::list &b = isfiltered ? unfiltered : filtered;
		wxXmlNode *node = a[index].node;

		// erase from first list
		a.erase(a.begin() + index);

		// find node in the second list and erase it
		// should be more efficient than rebuilding the second list
		item::list::iterator it = std::find_if(b.begin(), b.end(),
				boost::bind(&item::data, _1) == node);

		if (it != b.end())
			b.erase(it);

		// erase from xml document
		if (!root->RemoveChild(node))
			log->w(tag, "remove: tried to remove inexistent node, something's not right.");

		delete node;
	}

	void zitem::remove(long index)
	{
		remove(index, false);
	}

	void zitem::remove(wxString id)
	{
		wxXmlNode *root;

		if (!getroot(&root))
			return;

		// find item by id
		item::list::iterator found = std::find_if(unfiltered.begin(), unfiltered.end(),
				boost::bind(&item::get, _1, "id") == id);

		if (found == unfiltered.end())
			return;

		// delete by index
		remove(found - unfiltered.begin());
	}

	void zitem::clear()
	{
		wxXmlNode *root;

		if (!getroot(&root))
			return;

		boost_foreach (const item &i, unfiltered)
			root->RemoveChild(i.node);

		filtered.clear();
		unfiltered.clear();
	}

	void zitem::filtered_remove(long index)
	{
		remove(index, true);
	}

	void zitem::filtered_clear()
	{
		wxXmlNode *root;

		if (!getroot(&root))
			return;

		boost_foreach (const item &i, filtered)
		{
			// find item in the unfiltered list and delete it
			for (item::list::iterator it = unfiltered.begin(); it != unfiltered.end(); it++)
			{
				if (it->node == i.node)
				{
					unfiltered.erase(it);
					break;
				}
			}

			root->RemoveChild(i.node);
		}

		filtered.clear();
	}

	bool zitem::getroot(wxXmlNode **proot)
	{
		// validate document and root node
		if (!doc)
			return false;

		wxXmlNode *root = doc->GetRoot();

		if (!root)
			return false;

		if (proot)
			*proot = root;

		return true;
	}

	void zitem::setinlist(item::list &list, const wxString &property, const wxString &value)
	{
		boost_foreach(item &i, list)
		{
			// iterate items list and edit the ones that have the property
			if (i.has(property))
				i.set(property, value);
		}
	}

	void zitem::set(const wxString &property, const wxString &value)
	{
		setinlist(unfiltered, property, value);
	}

	void zitem::filtered_set(const wxString &property, const wxString &value)
	{
		setinlist(filtered, property, value);
	}

	void zitem::refreshfiltered()
	{
		// TODO: find a more efficient method to update the filtered list
		filtered.clear();

		boost_foreach (const item &i, unfiltered)
		{
			// add to both lists
			if (matchfilters(i))
				filtered.push_back(i);
		}
	}

	void zitem::addfilter(int id, const wxString &prop, const wxString &value)
	{
		filter f;
		f.id = id;
		f.property = prop;
		f.value = value;
		filters.push_back(f);

		refreshfiltered();
	}

	zitem::filter *zitem::getfilterbyid(int id)
	{
		boost_foreach (filter &f, filters)
		{
			if (f.id == id)
				return &f;
		}

		return NULL;
	}

	bool zitem::removefilterbyid(int id)
	{
		for (filter::iterator it = filters.begin(); it != filters.end(); it++)
		{
			if (it->id == id)
			{
				filters.erase(it);
				return true;
			}
		}

		if (!filters.size())
		{
			// no filters? show unfiltered items
			filtered.clear();
			filtered.insert(filtered.begin(), unfiltered.begin(), unfiltered.end());
		}

		return false;
	}

	bool zitem::setfilter(int id, const wxString &prop, const wxString &value)
	{
		// find filter
		filter *f = getfilterbyid(id);

		if (!f)
			return false;

		// update filter
		f->property = prop;
		f->value = value;

		refreshfiltered();

		return true;
	}

	wxString zitem::getfiltername(int id)
	{
		filter *f = getfilterbyid(id);

		if (!f)
			return wxEmptyString;

		return f->property;
	}

	wxString zitem::getfiltervalue(int id)
	{
		filter *f = getfilterbyid(id);

		if (!f)
			return wxEmptyString;

		return f->value;
	}

	bool zitem::removefilter(int id)
	{
		filter *f = getfilterbyid(id);

		// find filter
		if (!f)
			return false;

		// remove filter from list
		removefilterbyid(id);

		refreshfiltered();

		return true;
	}

	wxString zitem::currentfilename()
	{
		return wxString(boost::filesystem::path(
			currentfile.ToStdString()).filename().string());
	}

	std::vector<wxString> *zitem::properties()
	{
		return &proplist;
	}

	void zitem::refreshproperties()
	{
		boost_foreach (const item &xmlitem, unfiltered)
		{
			property p = xmlitem.getproperties();

			while (p.valid())
			{
				// if the property is not already on the prop list...
				if (std::find_if(proplist.begin(), proplist.end(),
						boost::bind(std::equal_to<wxString>(), _1, p.name()))
					== proplist.end())
				{
					proplist.push_back(p.name());
				}

				p = p.next();
			}
		}
	}

	size_t zitem::filtercount()
	{
		return filters.size();
	}
	// end zitem
	// }}}
}
